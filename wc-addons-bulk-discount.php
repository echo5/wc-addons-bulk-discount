<?php

/**
 * The plugin bootstrap file
 *
 * @link              https://echo5digital.com
 * @since             1.0.0
 * @package           Wc_Addons_Bulk_Discount
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce Add-ons Bulk Discount
 * Plugin URI:        #
 * Description:       Provide discounts on add-ons based on bulk quantity for the main product.
 * Version:           1.0.0
 * Author:            Joshua Flowers
 * Author URI:        https://echo5digital.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wc-addons-bulk-discount
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WC_ADDONS_BULK_DISCOUNT', '1.0.1' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wc-addons-bulk-discount.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wc_addons_bulk_discount() {

	$plugin = new Wc_Addons_Bulk_Discount();
	$plugin->run();

}
run_wc_addons_bulk_discount();
