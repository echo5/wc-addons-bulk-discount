=== WooCommerce Add-ons Bulk Discount ===
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin adds discounts to WC Addon options when the product quantity exceeds a minimum amount.

== Description ==

Apply discounts to WooCommerce Addon options when buying in bulk.  Discounts are applied realtime on 
the product page and also added to the total order amount.

This plugin also adds a dependency field to all options.

== Installation ==

1. Upload `wc-addons-bulk-discount/` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Apply discounts under checkbox and radio options in the `Addons` tab when editing products.

== Frequently Asked Questions ==

= How are discounts calculated? =

The plugin will automatically find the discount by the largest minimum quantity, not necessarily the
largest discount amount.  For example, if you have a minimum of 250 with a discount of 10 and another
discount with a minimum of 500 and a discount of 5, the total discount will be 5 if the minimum quantity
exceeds 500.

The discount value is removed from the original price value.  So if the option is $10 and the discount
value is 2, the total cost of the option if it meets the minimum would be $8.

= Can any addon contain a bulk discount? =

Currently, only those with options (radios and checkboxes), can have bulk discounts attached.

= How do dependencies work? =

If an addon has filled in its `Dependency` field, it will not show unless that dependency is added on
the product.

= How do you set dependencies? =

While any addon can have a dependency, the item it's depedent on must be a checkbox or radio addon.

To make `Addon A` only show when `Addon B` Option C is selected, you would fill in the label for this option
(e.g. `Option C`) under the `Addon A` dependency field.