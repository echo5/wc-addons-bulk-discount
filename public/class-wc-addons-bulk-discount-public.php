<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://echo5digital.com
 * @since      1.0.0
 *
 * @package    Wc_Addons_Bulk_Discount
 * @subpackage Wc_Addons_Bulk_Discount/public
 */

class Wc_Addons_Bulk_Discount_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wc-addons-bulk-discount-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wc-addons-bulk-discount-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'wcabd', array(
			'ajaxUrl' => admin_url( 'admin-ajax.php', $protocol ),
			'ajaxNonce' => wp_create_nonce( 'wcabd' ),
			'productId' => get_the_ID()
		) );

	}

	/**
	 * Get WC Addon templates
	 *
	 * @param string $located
	 * @param string $template_name
	 * @param array $args
	 * @param string $template_path
	 * @param string $default_path
	 * @return string
	 */
	public function get_addon_templates( $located, $template_name, $args, $template_path, $default_path ) {    
		if ( 'addons/checkbox.php' == $template_name ) {
			$located = plugin_dir_path( __FILE__ ) . 'addons/checkbox.php';
		} elseif ( 'addons/radiobutton.php' == $template_name ) {
			$located = plugin_dir_path( __FILE__ ) . 'addons/radiobutton.php';
		} elseif ( 'addons/addon-start.php' == $template_name ) {
			$located = plugin_dir_path( __FILE__ ) . 'addons/addon-start.php';
		} elseif ( 'addons/input_multiplier.php' == $template_name ) {
			$located = plugin_dir_path( __FILE__ ) . 'addons/input_multiplier.php';
		}
		return $located;
	}

	
	/**
	 * Adjust the price based on quantity
	 *
	 * @param array $cart_item
	 * @return array
	 */
	public function adjust_addon_cart_price( $data, $addon, $product_id, $post_data ) {
		foreach ( $addon['options'] as $option ) {
			if ( $option['label'] == $data[0]['value'] || $addon['name'] . ' - ' . $option['label'] == $data[0]['name'] ) {
				$discount = $this->get_discount_price( $option['bulk_discounts'], $post_data['quantity'] );
				if ( $addon['type'] == 'input_multiplier' ) {
					$discount = $discount * intval( $data[0]['value'] );
				}
				$data[0]['price'] = $discount ? $data[0]['price'] - (float) $discount : $data[0]['price'];
				break;
			}
		}
		return $data;
	}

	/**
	 * Get discounted price from list of discounts
	 *
	 * @param array $bulk_discounts
	 * @param integer $qty
	 * @return string
	 */
	public function get_discount_price( $bulk_discounts, $qty ) {

		$discount = 0;
		foreach ( $bulk_discounts as $key => $bulk_discount ) {
			if ( $qty >= $bulk_discount['minimum'] && ( !$discount || $bulk_discount['minimum'] > $discount['minimum'] ) ) {
				$discount = $bulk_discount;
			}
		}
		return $discount['value'];

	}

	/**
	 * Get product discount via ajax
	 *
	 * @return void
	 */
	public function get_product_discount() {

		check_ajax_referer( 'wcabd', 'security' );
		$discount = $this->get_discounted_coeff( intval( $_POST['productId'] ), intval( $_POST['quantity'] ) );
		wp_die( $discount );

	}

	/**
	 * Returns the final amount after discount
	 *
	 * @param int $product_id
	 * @param int $quantity
	 * @return float
	 */
	public function get_discounted_coeff( $product_id, $quantity ) {

		$q = array( 0.0 );
		$d = array( 0.0 );

		$configurer = get_page_by_title( 'wc_bulk_discount_configurer', OBJECT, 'product' );
		if ( $configurer && $configurer->ID && $configurer->post_status == 'private' ) {
			$product_id = $configurer->ID;
		}

		$product = wc_get_product($product_id);
		if ($product instanceof WC_Product_Variation) {
			$product_id = $product->get_parent_id();
		}
		
		/* Find the appropriate discount coefficient by looping through up to the five discount settings */
		for ( $i = 1; $i <= 5; $i++ ) {
			array_push( $q, get_post_meta( $product_id, "_bulkdiscount_quantity_$i", true ) );
			if ( get_option( 'woocommerce_t4m_discount_type', '' ) == 'flat' ) {
				array_push( $d, get_post_meta( $product_id, "_bulkdiscount_discount_flat_$i", true ) ? get_post_meta( $product_id, "_bulkdiscount_discount_flat_$i", true ) : 0.0 );
			} else if ( get_option( 'woocommerce_t4m_discount_type', '' ) == 'fixed' ) {
				array_push( $d, get_post_meta( $product_id, "_bulkdiscount_discount_fixed_$i", true ) ? get_post_meta( $product_id, "_bulkdiscount_discount_fixed_$i", true ) : 0.0 );
			} else {
				array_push( $d, get_post_meta( $product_id, "_bulkdiscount_discount_$i", true ) ? get_post_meta( $product_id, "_bulkdiscount_discount_$i", true ) : 0.0 );
			}
			if ( $quantity >= $q[$i] && $q[$i] > $q[0] ) {
				$q[0] = $q[$i];
				$d[0] = $d[$i];
			}
		}
		
		// for percentage discount convert the resulting discount from % to the multiplying coefficient
		if ( get_option( 'woocommerce_t4m_discount_type', '' ) == 'fixed' ) {
			// return max( 0, $d[0] * $quantity );
			return max( 0, $d[0] );
		}
		return ( get_option( 'woocommerce_t4m_discount_type', '' ) == 'flat' ) ? max( 0, $d[0] ) : min( 1.0, max( 0, ( 100.0 - round( $d[0], 2 ) ) / 100.0 ) );

	}
}
