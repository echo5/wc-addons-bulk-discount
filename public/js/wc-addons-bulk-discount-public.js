(function( $ ) {
	'use strict';

	/**
	 * Set original prices on items
	 */
	$( document ).ready( function() {
		$( '.addon' ).each( function() {
			$( this ).data( 'original-price', $( this ).data( 'price' ) );
		} );
		$( '#product-addons-total' ).data( 'original-price', $( '#product-addons-total' ).data( 'price' ) );
	});

	/**
	 * Update the prices on quantity change
	 */
	$( document ).on( 'change', 'input[name=quantity]', function() {
		var qty = $( this ).val();
		$( '.addon' ).each( function() {
			var originalPrice = $( this ).data( 'original-price' );
			var discount = getDiscount( $( this ).data( 'bulk-discounts' ), qty );
			if ( discount ) {
				var newPrice = parseFloat( originalPrice - discount['value'] );
				$( this ).data( 'raw-price', newPrice.toString() );
				$( this ).data( 'price', newPrice.toString() );
				var amount = $( this ).parent().find( '.amount' )[0];
				if ( amount ) {
					amount.childNodes[1].nodeValue = parseFloat( newPrice ).toFixed(3).toString();
				}
			} else {
				$( this ).data( 'raw-price', originalPrice );
				$( this ).data( 'price', originalPrice );
				var amount = $( this ).parent().find( '.amount' )[0];
				if ( amount ) {
					amount.childNodes[1].nodeValue = originalPrice;
				}
			}
		} );
		$('.cart').trigger('woocommerce-product-addons-update');
	});

	/**
	 * Get the maximum discount object based on quantity
	 * @param {object} discounts 
	 * @param {integer} qty 
	 */
	function getDiscount( discounts, qty ) {
		var max = false;
		$.each( discounts, function( i, item ) {
			if ( qty >= item['minimum'] && ( !max || item['minimum'] > discounts[max]['minimum'] )) {
				max = i;
			}
		});
		return max !== false ? discounts[max] : false;
	}

	/**
	 * Watch for checkbox/radio depedency changes
	 */
	$( document ).ready( function() {
		$( 'input[type=checkbox], input[type=radio' ).change( function() {
			var name = $( this ).parent().text().replace(/\s*\(.*?\)\s*/g, '').replace(/\W/g,'').trim();
			if( $( this ).is(':checked') ) {
				$( 'div[data-dependency="' + name + '"]' ).show();
			} else {
				$( 'div[data-dependency="' + name + '"]' ).hide();
			}
		});
	});

	/**
	 * Trigger changes for all radio inputs when deselected
	 */
	$( document ).ready( function() {
		$( 'input[type=radio]' ).change( function (e) {
			var targetRadios = $('input[name="' + $( this ).attr('name') + '"]');
			if (e.hasOwnProperty('originalEvent')) {
				targetRadios.each(function() {
					$(this).triggerHandler( 'change' );
				});
			}
		});
	});

	/**
	 * Trigger product price changes when quanityt changed
	 */
	$( document ).on( 'change', 'input[name=quantity]', function() {
		var productId = parseInt( $( this ).closest( '.type-product' ).attr( 'id' ).replace( 'product-', '' ) );
		var data = {
			action: 'get_product_discount',
			quantity: $( this ).val(),
			productId: productId,
			security: wcabd.ajaxNonce
		}
		$.ajax({
			type : 'post',
			dataType : 'json',
			url : wcabd.ajaxUrl,
			data : data,
			success: function(response) {
				var newPrice = $( '#product-addons-total' ).data( 'original-price' ) - response;
				$( '#product-addons-total' ).data( 'price', newPrice );
				$( '#product-addons-total' ).data( 'raw-price', newPrice );
				$('.cart').trigger('woocommerce-product-addons-update');
				$( '.woocommerce-Price-amount.amount' )[0].childNodes[1].nodeValue = parseFloat( newPrice ).toFixed(3).toString();
			}
		});
	});


})( jQuery );