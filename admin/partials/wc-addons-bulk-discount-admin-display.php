<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://echo5digital.com
 * @since      1.0.0
 *
 * @package    Wc_Addons_Bulk_Discount
 * @subpackage Wc_Addons_Bulk_Discount/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
