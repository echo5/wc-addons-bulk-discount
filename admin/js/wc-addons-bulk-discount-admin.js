(function( $ ) {
	'use strict';

	/**
	 * Add new discount
	 */
	$('.add_addon_bulk_discount').click( function(e) {
		e.preventDefault();
		var fields = $(this).prev();
		var clone = fields.clone();
		clone.insertAfter(fields);
		clone.find( 'input' ).val( '' );
	});

	/**
	 * Remove discount
	 */
	$(document).on( 'click', '.remove_addon_bulk_discount', function(e) {
		e.preventDefault();
		$(this).parent().remove();
	});

})( jQuery );
