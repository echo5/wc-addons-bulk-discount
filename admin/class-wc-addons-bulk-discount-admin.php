<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://echo5digital.com
 * @since      1.0.0
 *
 * @package    Wc_Addons_Bulk_Discount
 * @subpackage Wc_Addons_Bulk_Discount/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wc_Addons_Bulk_Discount
 * @subpackage Wc_Addons_Bulk_Discount/admin
 * @author     Joshua Flowers <joshua@echo5digital.com>
 */
class Wc_Addons_Bulk_Discount_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wc_Addons_Bulk_Discount_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wc_Addons_Bulk_Discount_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wc-addons-bulk-discount-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wc_Addons_Bulk_Discount_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wc_Addons_Bulk_Discount_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wc-addons-bulk-discount-admin.js', array( 'jquery' ), $this->version, true );

	}

	/**
	 * Show the bulk discount option fields under addon options
	 *
	 * @param array $post
	 * @param array $product_addons
	 * @param integer $loop
	 * @param array $option
	 * @return void
	 */
	public function add_bulk_discount_fields_to_options( $post, $product_addons, $loop, $option ) {
		global $option_loop, $current_addon_loop;
		$option_loop = ( isset( $current_addon_loop ) && $current_addon_loop == $loop ) ? $option_loop + 1 : 0;
		$current_addon_loop = $loop;
		ob_start();
		if ( !isset( $option['bulk_discounts'] ) || count( $option['bulk_discounts'] ) < 1 ) {
			$option['bulk_discounts'] = array( array( 'minimum' => '', 'value' => '' ) );
		}
		?>
		<td class="wc-addon-bulk-discount-discounts">
			<?php foreach ($option['bulk_discounts'] as $discount): ?>
				<div class="wc-addon-bulk-discount-fields">
					<div class="wc-addon-bulk-discount-field">
						<input type="text" placeholder="<?php _e( 'Minimum Quantity', 'wc-addons-bulk-discount' ); ?>" name="product_addon_option_bulk_discount_minimum[<?php echo $loop; ?>][<?php echo $option_loop; ?>][]" value="<?php echo $discount['minimum']; ?>" />
					</div>
					<div class="wc-addon-bulk-discount-field">
						<input type="text" placeholder="<?php _e( 'Discount Value', 'wc-addons-bulk-discount' ); ?>" name="product_addon_option_bulk_discount_value[<?php echo $loop; ?>][<?php echo $option_loop; ?>][]" value="<?php echo $discount['value']; ?>" />
					</div>
					<button class="remove_addon_bulk_discount button">x</button>
				</div>
			<?php endforeach; ?>
			<button type="button" class="button add_addon_bulk_discount"><?php _e( 'Add discount', 'wc-addons-bulk-discount' ); ?></button>
		</td>
		<?php
		$output = ob_get_clean();
		echo $output;
	}

	/**
	 * Add table header col for spacing
	 *
	 * @param array $post
	 * @param array $addon
	 * @param integer $loop
	 * @return void
	 */
	public function add_addon_table_header( $post, $addon, $loop ) {
		echo '<th>'.  __( 'Bulk Discounts', 'wc-addons-bulk-discount' ) . '</th>';
	}

	/**
	 * Save bulk discount fields
	 *
	 * @param array $post
	 * @param array $addon
	 * @param integer $loop
	 * @return void
	 */
	public function save_bulk_discount_fields( $data, $i ) {
		for ( $ii = 0; $ii < sizeof( $data['options'] ); $ii++ ) {
			$minimum = $_POST['product_addon_option_bulk_discount_minimum'][$i];
			$value = $_POST['product_addon_option_bulk_discount_value'][$i];
			$discounts = array();
			for ( $iii = 0; $iii < sizeof( $minimum[$ii] ); $iii++ ) {
				if (intval( $minimum[$ii][$iii] ) > 0) {
					$discounts[] = array(
						'minimum' => intval( $minimum[$ii][$iii] ),
						'value' => floatval( $value[$ii][$iii] )
					);
				}
			}
			$data['options'][$ii]['bulk_discounts'] = $discounts;
		}
		return $data;
	}

	/**
	 * Show the dependency field for each addon
	 *
	 * @param array $post
	 * @param array $product_addons
	 * @param integer $loop
	 * @param array $option
	 * @return void
	 */
	function add_dependency_fields( $post, $addon, $loop ) {
		?>
		<tr>
			<td>
				<label for="addon_dependency_<?php echo $loop; ?>">
					<?php
						_e( 'Dependency', 'wc-addons-bulk-discount' );
					?>
				</label>
				<input type="text" id="addon_dependency_<?php echo $loop; ?>"  name="product_addon_dependency[<?php echo $loop; ?>]" value="<?php echo isset( $addon['dependency'] ) ? sanitize_text_field( $addon['dependency'] ) : '' ?>" />
			</td>
		</tr>
		<?php
	}

	/**
	 * Save dependency fields
	 *
	 * @param array $post
	 * @param array $addon
	 * @param integer $loop
	 * @return void
	 */
	function save_dependency_fields( $data, $i ) {
		$dependency = $_POST['product_addon_dependency'][$i];
		$data['dependency'] = esc_sql( $dependency );
		return $data;
	}

}
